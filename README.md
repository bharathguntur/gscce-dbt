# gda-dbt-template

**gda-dbt-template** is a shell project to help teams at Grainger get started with publishing data to GDA for broader consumption.  This repo contains the basic project structure required to get started as well as a [Getting Started](#markdown-header-getting-started) guide with directions on how to configure the project and deploy your first model.

Before using this repo, you will need to make sure you are properly onboarded to use this capability.  If you haven't done so, please **[submit a request](https://gprod.service-now.com/wwg?id=sc_cat_item&sys_id=db59bfa91b72cc18c998a6c8ec4bcbf8)** to the **GDA** team in order to be onboarded.

## Table of Contents

* [Getting Started](#markdown-header-getting-started)
    * [Pre-Requisites](#markdown-header-pre-requisites)
    * [Fork This Repository](#markdown-header-fork-this-repository)
    * [Grant Read Access to `gda-service`](#markdown-header-grant-read-access-to-gda-service)
    * [Set Up Automated Deployment](#markdown-header-set-up-automated-deployment)
    * [Configure Your Project](#markdown-header-configure-your-project)
    * [Usage](#markdown-header-usage)
        * [Validate Setup](#markdown-header-validate-setup)
        * [Create a New Model](#markdown-header-create-a-new-model)
        * [Document a Model](#markdown-header-document-a-model)
        * [Mask Columns](#markdown-header-mask-columns)
        * [Test Deployment with a Team Database](#markdown-header-test-deployment-with-a-team-database)
        * [Deploy a Model to the PUBLISH Database](#markdown-header-deploy-a-model-to-the-publish-database)
        * [Other](#markdown-header-other)
* [FAQ](#markdown-header-faq)
* [Contributing](#markdown-header-contributing)

## Getting Started

> **NOTE**: If, after reading through the **Getting Started** guide, you would like help getting set up, please reach out to the **GDA** team on Slack: [**#gda_support**](https://grainger.slack.com/archives/CNN7BMD1C) or Teams: [**GDA Support**](https://teams.microsoft.com/l/channel/19%3a5dbf64e9b59b47d8bc6ed1fece7e243b%40thread.skype/GDA%2520Support?groupId=a201d911-430c-4e50-a44d-71f15b59fe26&tenantId=48d1dcb6-bccc-4365-ac7f-b937a7f7fd71), and ask to set up an onboarding session.  Please plan to have completed all [Pre-requisites](#markdown-header-pre-requisites) prior to your session.

### Pre-Requisites

* Read and agree to the GDA [Terms of Use](https://confluence.grainger.com/display/GDA/TERMS+OF+USE)
* A GDA Team Database and permissions to Publish
    * You can [submit a request](https://gprod.service-now.com/wwg?id=sc_cat_item&sys_id=db59bfa91b72cc18c998a6c8ec4bcbf8) for access in SNOW
        * Select **Other requests** from the dropdown menu and use the text box to indicate that you would like access to publishing.  Please provide the folowing:
            * Description of use case
            * Name of your Team Database
            * GDA will reach out to coordinate any additional details
* Access to [Bitbucket](https://www.bitbucket.org) and [CircleCI](https://www.cirleci.com)
    * Access can be requested in the [#pe_support](https://grainger.slack.com/archives/C01AW5DH878) channel in Slack
        * Click on the blue lightning bolt on the bottom left and select **Access Request**
* Familiarity with `git`
    * Tutorials can be found [here](https://www.atlassian.com/git/tutorials)
* **[OPTIONAL]** Create a service user for your team in Bitbucket

    * Create a team email distribution/address (ie `gda-service@grainger.com`)
    * Create a [Bitbucket](https://www.bitbucket.org) account using this email address
    * Submit a request in [#pe_support](https://grainger.slack.com/archives/C01AW5DH878) to add the new user to your team in Bitbucket

* **[OPTIONAL]** [Install](https://docs.getdbt.com/dbt-cli/installation) DBT locally

### Fork this Repository

Creating a fork of this repo will allow you to modify the files included here in order to begin publishing.  To create a fork, follow these steps:

1. Click on the **+** in the leftmost global sidebar and select **Fork this repository** at the bottom of the lists

2. In the **Fork** dialog, define the otpions for your fork

    **Workspace**: This defaults to the logged-in account, which should be `wwgrainger`.  If not, select `wwgrainger` from the drop-down menu

    **Project**: Your team should have a project within the `wwgrainger` workspace.  Ensure that your team's project is selected (for example, GDA's project is *Grainger Data & Analytics*)

    **Name**: This is the name the forked repo will have.  This can be anything you want, but replacing `<team>-dbt` with your team name or initials is a good option (ie `gda-dbt`)

    **Access Level**: This will default to a `private` repo

    **Forking**: Under `Advanced settings`, choose whether you want to allow only private forks

3. Click **Fork repository**.  The system will create the fork and open the repository's **Source** page.

### Grant Read Access to `gda-service`

Once the repository is forked, you will need to grant `read` access to the `gda-service` user so that any models you define can be deployed by the build pipeline.  To grant access:

1. Click on **Repository Settings**
2. Click on **User and Group Access**
3. Under the `Users` section, search for the `gda-service` user and select it
4. Select `Read` from the dropdown menu and then click **Add**

### Set Up Automated Deployment

#### Link Your Repo with CircleCI

Linking your new repo with CircleCI will allow you to trigger automated deployments to Snowflake in all environments (dev/qa/prod).  This project template contains pre-defined configuration for CircleCI.  The [Configure Your Project](#markdown-header-configure-your-project) section of this guide will explain how to modify the CircleCI configuration file.

1. Navigate to the [CircleCI Project Dashboard](https://app.circleci.com/projects/project-dashboard/bitbucket/wwgrainger/)

2. Search for your project in the search box

3. Once you've found your project, click on **Set Up Project**

4. In the _Sample configs_ dialog box, click **Skip this step** (this template already has a configured pipeline)

5. On the next page, click **Use Existing Config**

6. In the dialog box, click **Start Building**

#### Create an API Key

Your team will need an API key to trigger the `gda-dbt-publish` pipeline via CircleCI.  To set up a key for your project, follow these steps:

##### Generate an API Key

1.  Login to CircleCI (**optional**: Use your Bitbucket service user created during the optional pre-requisite step above).
2.  Click on [User Settings](https://circleci.com/account).
3.  Click [Personal API Tokens](https://circleci.com/account/api).
4.  Click the **Create New Token** button.
5.  Click the **Add API Token** button.
6.  After the token appears, copy and paste it to another location. You will not be able to view the token again.

##### Set the API Key

1.  In the CircleCI application, go to your project's settings by clicking the gear icon on the Pipelines page, or the three dots on other pages in the application
2.  Click on Environment Variables.
3.  Add a new variable by clicking the Add Variable button and enter the following details:

    * **Name**:  CIRCLECI_API_KEY
    * **Value**: <YOUR_API_KEY>

Once created, environment variables are hidden and uneditable in the application. Changing an environment variable is only possible by deleting and recreating it.

#### Follow the `gda-dbt-publish` Project

The `gda-dbt-publish` build pipeline is triggered at the end of the build pipeline for your project.  Following `gda-dbt-publish` will allow you to monitor and manage progress for your deployment.

1.  Navigate back to the [CircleCI Project Dashboard](https://app.circleci.com/projects/project-dashboard/bitbucket/wwgrainger/)

2. Search for `gda-dbt-publish`

3. Click **Follow Project**

### Configure Your Project

1. Configure `dbt_project.yml`

    Set the following values in the `dbt_project.yml` file:

    1. `name` - This will be the name of your DBT project.  Other teams may import or use your models, and this name will be how your models are referenced by those teams and in DBT documentation.
    2. `profile` - This is the default profile that DBT will use when you run DBT locally.  Setting the name to your team name is probably simplest (ie `gda`)
    3. `<team>_dbt` - Replace `<team>_dbt` under `models:` with the value that you set for `name` in this file
    4. `<team>` - Replace `<team>` under `+materialized: view` with your team name (ie `gda`)
    5.  `<schema>` - Replace `<schema>` with the schema you'd like to deploy models to for manual testing in you Team Database. In the PUBLISH database, all team models will be deployed to a schema with a name that matches that of your Team Database (ie `GDA`)


    ```bash
    ---
    name: '<team>_dbt' # Ex. gda_dbt
    version: '1.0.0'
    config-version: 2

    profile: '<team>' # Ex. gda

    source-paths: ["models"]
    analysis-paths: ["analysis"]
    test-paths: ["tests"]
    data-paths: ["data"]
    macro-paths: ["macros"]
    snapshot-paths: ["snapshots"]

    target-path: 'target'
    clean-targets:
      - 'target'
      - 'dbt_modules'

    models:
      <team>_dbt: # Replace this with whatever value you set for name above.  Ex. gda_dbt
        publish:
          +materialized: view
          <team>: # Ex. gda
            +schema: <schema> # Ex. gda
    ```

    For more details about the `dbt_project.yml` file, review the official DBT documentation: [dbt project configuration](https://docs.getdbt.com/reference/dbt_project.yml)

2. Configure a local DBT profile

    The default location for DBT profiles is `~/.dbt/profiles.yml`.  You can customize the location as well.  For more info on configuring profiles, check out the documentation: [dbt profiles](https://docs.getdbt.com/docs/profile)

    This local profile will only be used for manual testing against your Team Database.  **GDA** generates a profile for your team when deploying any models to Snowflake.

    Set the following values for each environment:

    1. `<team>` - Replace `<team>` with the value you set for `profile` in `dbt_project.yml`
    1. `user` - This should be set to your Team Database SVC user (ie `GDA_SVC`)
    2. `password` - Set this to the password for your Team Database SVC user for the given environment
    3. `role` - Set this to the role that should be assumed when dbt is run.  This should match the name of your Team Database SVC user (ie `GDA_SVC`)
    4. `database` - Set this to the name of your Team Database (ie `GDA`)
    5. `warehouse` - Set this to the name of your Team Database Warehouse (ie `GDA_WH`)
    6. `schema` - Set this to the schema where you want DBT to build any models.  In the **PUBLISH** database, all team models will be deployed to a schema with a name that matches that of your Team Database (ie `GDA`)

    ```bash
    # For more information on how to configure this file, please see:
    # https://docs.getdbt.com/docs/profile

    <team>: # Ex. gda - NOTE: THis should match whatever you set for "profile" in dbt_project.yml
      outputs:
        dev:
          type: snowflake
          account: wwgraingerdev.us-east-1
          user: <TEAM>_SVC # Ex. GDA_SVC
          password: <password> # password123
          role: <TEAM>_SVC # Ex. GDA_SVC
          database: <TEAM> # Ex. GDA
          warehouse: <TEAM>_WH #Ex. GDA_WH
          schema: <SCHEMA> # Ex. GDA
          threads: 1
          client_session_keep_alive: False
        qa:
          type: snowflake
          account: wwgraingerqa.us-east-1
          user: <TEAM>_SVC # Ex. GDA_SVC
          password: <password> # password123
          role: <TEAM>_SVC # Ex. GDA_SVC
          database: <TEAM> # Ex. GDA
          warehouse: <TEAM>_WH #Ex. GDA_WH
          schema: <TEAM> # Ex. GDA
          threads: 1
          client_session_keep_alive: False
        prod:
          type: snowflake
          account: wwgrainger.us-east-1
          user: <TEAM>_SVC # Ex. GDA_SVC
          password: <password> # password123
          role: <TEAM>_SVC # Ex. GDA_SVC
          database: <TEAM> # Ex. GDA
          warehouse: <TEAM>_WH #Ex. GDA_WH
          schema: <TEAM> # Ex. GDA
          threads: 1
          client_session_keep_alive: False
      target: dev
    ```

3. Rename the `models/publish/team_name` directory in this project

    > **NOTE**: The name of the directory should match the name you set for `<team>` under **models.publish** in the `dbt_project.yml` file

    ```bash
    ├── README.md
    ├── analysis
    ├── data
    ├── dbt_modules
    ├── dbt_project.yml
    ├── logs
    ├── macros
    │   └── generate_schema_name.sql
    ├── models
    │   └── publish
    │       └── team_name # Ex. gda
    ├── packages.yml
    ├── snapshots
    ├── target
    └── tests
    ```

4. Configure the `.circleci/config.yml` file

    This file controls the deployment of your models to Snowflake.

    1. `team-project` - Set the value of `team-project` to match the value of `name` from your `dbt_project.yml` file
    2. `team` - Set the value of `team` to match the name of your Team Database/Team Name in GDA

    ```bash
    ---
    version: 2.1

    orbs:
      dbt-publish: wwgrainger/dbt-publish@1.0.0

    workflows:
      version: 2
      trigger-dbt-publish:
        jobs:
          - dbt-publish/trigger-dbt-publish-pipeline:
              team-name: <team> # Ex. gda
              team-project: <team-project> # Ex. gda_dbt
              name: trigger-dbt-publish-pipeline
    ```

## Usage

### Validate Setup

To validate your setup locally, run the following steps:

#### Pull Dependencies

The models published by the GDA team are available in the `gda-dbt-core` project.  The `packages.yml` file in this repo references GDA's project and all model definitions can be imported for use in this project, similar to importing a library in a programming language.  To import the GDA models, run the following:

```bash
$ dbt deps
```

> **NOTE**: All GDA models will be imported into the `dbt_modules` directory.  This step is required for validating any models that reference models in the **ANALYTICS** database locally.  This step runs automatically as part of the deployment pipeline.

#### Compile

Compile ensures that all model files can correctly compile to executable SQL and that all dependencies exist and are able to be referenced.  To run compile, execute the following:

```bash
$ dbt compile
```

### Create a New Model

In dbt, a model is a `select` statement. Models are defined in `.sql` files (typically in your **models** directory):

* Each `.sql` file contains one model / `select` statement
* The name of the file is used as the model name

    > **NOTE**: Model names must be unique across all projects that are deployed to the GDA environment.  GDA recommends naming your models with the following convention `<team_name>.<model_name>.sql` to help ensure uniqueness

* Models can be nested in subdirectories within the models directory

When dbt runs, it will build any models defined by your team in the **PUBLISH** database by wrapping it in a `CREATE VIEW AS` or `CREATE TABLE AS` statement.

This is an example of a model file:

`gda.test_model.sql`
```sql
-- By default, DBT will use the model name as defined in the file name to create the table or view in Snowflake
-- If an alias is specified, the alias will be used instead.  GDA recommends specifying an alias if you
-- have named your model according to the convention <team_name>.<model_name>.sql
{{ config(alias='test_model') }}

SELECT
  DATE_TIME,
  USERNAME AS REPORT_SUITE_ID,
  MCVISID AS MARKETING_CLOUD_VISITOR_ID
FROM {{ ref('gda_dbt_core', 'adobe_clickstream.hits_daily_v') }}
-- Use {{ ref() }} instead of
-- FROM ANALYTICS.ADOBE_CLICKSTREAM.HITS_DAILY_V
```

#### The `ref()` function

`ref()` is the most important function in dbt, and it is repsonsible for two primary tasks.  It interpolates the schema for the model file, which allows us to change where the model is deployed via configuration, and the references between models are what allow DBT to properly build the lineage graph and deploy models in the correct order.

> **NOTE**: It is possible to create models in DBT that do not use `ref()`.  However, GDA has implemented rules in the build pipeline to ensure that `ref()` is used properly.  Any models without `ref()` will fail to deploy to the **PUBLISH** database.

For more information on the `ref()` function, check out the DBT [ref documentation](https://docs.getdbt.com/reference/dbt-jinja-functions/ref)

### Document a Model

You can optionally document your model and provide information about the model and the columns it contains.  Model docs are configured in `yml` files.

`test_model.yml`
```bash
version: 2

models:
  - name: gda.test_model
    description: A description of my model
    columns:
      - name: date_time
        description: A description for the date_time field
      - name: report_suite_id
        description: A description for the report_suite_id field
      - name: marketing_cloud_visitor_id
        description: A description for the marketing_cloud_visitor_id field
```

For more details on the supported options for documentation, see the DBT documentation for [model properties](https://docs.getdbt.com/reference/model-properties)

### Mask Columns

Access to data inside of the **ANALYTICS** and **PUBLISH** databases is managed through [Dynamic Data Masking](https://docs.snowflake.com/en/user-guide/security-column-ddm-intro.html).  Instead of limiting access to specific models, users will be able to see all models in **PUBLISH** and **ANALYTICS**, and the data within those models will be dynamically masked/shown based upon the role of the user querying the data.

The **GDA** team, in coordination with the **Data Management & Governance (DMG)** team, use DBT to configure the masking policies applied to columns in the **ANALYTICS** database.  As a team publising data to the **PUBLISH** database, you are responsible for appropriately tagging columns based upon the guidelines established by **DMG**.  For more details on the tagging guidlines, please reference the following documentation

* [**GDA Masking Policy Tags**](https://confluence.grainger.com/pages/viewpage.action?spaceKey=GDA&title=GDA+Data+Masking+Tags) - A list of all currently supported masking policy tags, the associated access tier, and their behavior
* [**DMG Access Model Documentation**](https://confluence.grainger.com/pages/viewpage.action?spaceKey=DMG&title=Snowflake+Documentation) - Guidelines to help determine which columns should be masked and at which access tier they belong.

Masking policies are applied to your models based upon tags that you apply in the model documentation (referenced above).  Here is an example with masking policies applied:

`test_model.yml`
```bash
version: 2

models:
  - name: gda.test_model
    description: A description of my model
    columns:
      - name: date_time
        description: A description for the date_time field
      - name: report_suite_id
        description: A description for the report_suite_id field
      - name: marketing_cloud_visitor_id
        description: A description for the marketing_cloud_visitor_id field
        tags:
          - med_string # This column will be masked with the MED_STRING masking policy
          - not_a_masking_tag # You can safely apply other tags if you wish
                              # and these will not be factored into masking policies
```

### Test deployment with a Team Database

If you have DBT installed locally, you can manually deploy your model to your Team Database to test it out.  To manually deploy, run the following steps:

1. Compile DBT

    ```bash
    # $ dbt compile --target <env>
    # Replace <env> with the desired Snowflake environment where you want to deploy your changes (dev/qa/prod)

    $ dbt compile --target dev
    ```
2. List your model

    > **NOTE**: This step isn't required, but will help you determine if you are using the correct syntax to select only the model(s) that you want to build.  For more details on selecting models with DBT, see the [node selection syntax](https://docs.getdbt.com/reference/node-selection/syntax) documentation.

    ```bash
    # $ dbt list --models <project_name>.<model_name>
    # Replace <project_name> with the value of "name" in dbt_project.yml and replace <model_name> with the
    # file name of model you want to build

    $ dbt list --models gda.test_model
    gda.test_model
    ```

3. Run your model

    > **WARNING**: Executing `dbt run` without specifying the model(s) that you want to build will result in ALL models being built, including the models imported via packages.  Unless you want to rebuild the entire **ANALYTICS** database inside your team database, it is strongly recommended that you limit `dbt run` with the `--models` parameter.

    ```bash
    # $ dbt run --models <project_name>.<model_name> --target <env>
    # Replace <project_name> with the value of "name" in dbt_project.yml and replace <model_name> with the
    # file name of model you want to build
    # Replace <env> with the desired Snowflake environment where you want to deploy your changes (dev/qa/prod)

    $ dbt run --models gda.test_model --target dev
    Found 1 models, 0 tests, 0 snapshots, 0 analyses, 326 macros, 0 operations, 0 seed files, 1 sources, 0 exposures

    11:55:40 | Concurrency: 1 threads (target='dev')
    11:55:40 |
    11:55:40 | 1 of 1 START view model gda.dbt_test.my_model [RUN]
    11:55:48 | 1 of 1 OK created view model gda.dbt_test.my_model [SUCCESS 1 in 7.91s]
    11:55:49 |
    11:55:49 | Finished running 1 view model in 18.65s.

    Completed successfully

    Done. PASS=1 WARN=0 ERROR=0 SKIP=0 TOTAL=1
    ```

    > **NOTE**: DBT does not automatically delete models.  Any models that you create in testing will need to manually be removed from your team database.

### Deploy a Model to the **PUBLISH** database

To start a new change, create a new git branch:

```bash
# $ git checkout -b <name-of-your-branch>

$ git checkout -b test-model
switched to new branch test-model
```

Once you are on your new branch, add a new model and documentation by creating a new model `.sql` file and adding any documentation to a new or existing `.yml` file as described in the [Create a New Model](#markdown-header-create-a-new-model), [Document a Model](#markdown-header-document-a-model) and [Mask Columns](#markdown-header-mask-columns) sections above.

#### Trigger Deploy to Dev Only

To test the model and documentation in dev, push your branch

```bash
# $ git push -u origin <name-of-your-branch>

$ git push -u origin test-model
```

#### Trigger Deploy To All Environments

To trigger the full build process for all environments, merge your branch to master.

#### Manage Your Deployment

Whether you are testing in dev or deploying to all environments, management of the build is the same.

1. Navigate to your project in [CircleCI](https://app.circleci.com/pipelines/bitbucket/wwgrainger)
2. You will see a build in progress (or completed) to `trigger-dbt-publish` for your branch.
3. Once the previous step is completed, navigate to the [gda-dbt-publish](https://app.circleci.com/pipelines/bitbucket/wwgrainger/gda-dbt-publish) project in CircleCI
4. A build will be in progress with a tag that follows the following naming convention:

    `<TEAM>|envs=<envs>|run-mode=<run_mode>|project=<project_name|branch=<branch>|build_number=<build_number>`

    The tag is made up of the following components:

    * **\<TEAM\>**: Matches the value set for `team-name` in the CircleCI config.yml
    * **envs**: Can be either `dev` or `all`.

        * `dev`: Deploys to Snowflake dev only.
        * `all`: Deploys to all Snowflake envs.

    * **run-mode**: Can be either `state_modified` or `full_refresh`

        * `state_modified` - [DEFAULT] Only models which have changed are deployed.  For more details on this, see the DBT docs:
        * `full_refresh` - All models are re-deployed.  Branches must be named as `full/*` to trigger a full refresh

    * **project**: Matches the value set for `team-project` in the CircleCI config.yml
    * **branch**: The name of the branch that triggered the build in your team's DBT project
    * **build_number**: A sequential build number

3. For a dev only build, the `dbt-deps-and-compile-dev` step should already be running.  If this completes successfully, you can view the dev database and documentation to validate.
4. For full builds, once `dbt-deps-and-compile-dev` is complete, validate results in dev and then approve deployment to QA using the `approve-qa-deploy` step in the build pipeline
5. Repeat the process for deployment to Prod

For any issues or failures in your build, start with the **FAQ** section of this document.  If you can't resolve the issue, reach out to the **GDA** team on Slack: [**#gda_support**](https://grainger.slack.com/archives/CNN7BMD1C) or Teams: [**GDA Support**](https://teams.microsoft.com/l/channel/19%3a5dbf64e9b59b47d8bc6ed1fece7e243b%40thread.skype/GDA%2520Support?groupId=a201d911-430c-4e50-a44d-71f15b59fe26&tenantId=48d1dcb6-bccc-4365-ac7f-b937a7f7fd71)

### Other
For more details on DBT and more detailed usage examples and configuration options, review the [DBT Documentation](https://docs.getdbt.com)

## FAQ

**Q**: What kind of models can I deploy to Snowflake?

**A**: GDA currently only supports models of the `view` type. We hope to support models of the `table` and `incremental` type in the future.

---

**Q**: How do I get access to publish my own views in GDA?

**A**: To gain access to GDA publishing, follow these steps:

1. Read the GDA [Terms of Use](https://confluence.grainger.com/display/GDA/TERMS+OF+USE)
2. [Submit a request](https://gprod.service-now.com/wwg?id=sc_cat_item&sys_id=db59bfa91b72cc18c998a6c8ec4bcbf8) in Service Now

    * Select **Other requests** from the dropdown menu and use the text box to indicate that you would like access to publishing.  Please provide the folowing:
      * Description of use case
      * Name of your Team Database
      * GDA will reach out to coordinate any additional details

---

**Q**: My deployment failed, how can I see what went wrong?

**A**: To troubleshoot a failed deployment, follow these steps:

1. Navigate to your project in [CircleCI](https://app.circleci.com/pipelines/bitbucket/wwgrainger) or to the [gda-dbt-publish](https://app.circleci.com/pipelines/bitbucket/wwgrainger/gda-dbt-publish) project in CircleCI depending on where the build failed
2. Click on the failed step in the build workflow
3. Click on the **Artifacts** tab
4. Click on `migrations/results.txt` to view the deployment logs

---

**Q**: My deployment failed because of an `olivertwist` validation error, how do I fix it?

**A**: [Olivertwist](http://olivertwi.st/) is a library designed for validation of DBT models and the lineage graph. GDA uses `olivertwist` to enforce the following rules as part of all builds:

1. No orphaned models: [docs](https://github.com/autotraderuk/oliver-twist/blob/main/RULES.md#no-orphaned-models)
2. No references to models outside of its own staging area: [docs](https://github.com/autotraderuk/oliver-twist/blob/main/RULES.md#no-references-outside-of-its-own-staging-area)
3. No references to marts from staging: [docs](https://github.com/autotraderuk/oliver-twist/blob/main/RULES.md#no-references-to-marts-from-staging)
4. No rejoin models: [docs](https://github.com/autotraderuk/oliver-twist/blob/main/RULES.md#no-rejoin-models)

View the documentation for any violations associated with your model and adjust accordingly

---

**Q**: My deployment failed because DBT found a model with the same name as mine, how do I fix it?

**A**: Model file names must be unique across all projects being deployed to Snowflake.  If your model file has a name that conflicts with another team's model, consider naming your model with the following convention `<team_name>.<model_name>.sql`.  If you use this convention, you will want to also set an `alias` within your model file:

`<team_name>.<model_name>.sql`
```sql
{{ config(alias='model_name') }}

-- MODEL SQL HERE
```

---

**Q**: My deployment failed because of a SQL compilation error, how do I fix it?

**A**: For any issues with SQL compilation, follow the steps above to test your model in your Team Database or manually test your SQL in Snowflake.  If the issue persists, reach out to the **GDA** team on Slack: [**#gda_support**](https://grainger.slack.com/archives/CNN7BMD1C) or Teams: [**GDA Support**](https://teams.microsoft.com/l/channel/19%3a5dbf64e9b59b47d8bc6ed1fece7e243b%40thread.skype/GDA%2520Support?groupId=a201d911-430c-4e50-a44d-71f15b59fe26&tenantId=48d1dcb6-bccc-4365-ac7f-b937a7f7fd71)


## Contributing
Pull requests are welcome. For major changes, please reach out o the **GDA** team on Slack: [**#gda_support**](https://grainger.slack.com/archives/CNN7BMD1C) or Teams: [**GDA Support**](https://teams.microsoft.com/l/channel/19%3a5dbf64e9b59b47d8bc6ed1fece7e243b%40thread.skype/GDA%2520Support?groupId=a201d911-430c-4e50-a44d-71f15b59fe26&tenantId=48d1dcb6-bccc-4365-ac7f-b937a7f7fd71) to talk through the proposed change in advance of submitting a PR.