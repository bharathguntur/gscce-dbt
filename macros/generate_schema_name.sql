-- This macro generates a schema for your model based upon the +schema
-- configuration in your dbt_project.yml.  Removing this macro may cause
-- unexpected behavior when testing against your Team Database.  It will
-- not impact deployment to the PUBLISH database.

{% macro generate_schema_name(custom_schema_name, node) -%}

    {%- set default_schema = target.schema -%}
    {%- if custom_schema_name is none -%}

        {{ default_schema }}

    {%- else -%}

        {{ custom_schema_name | trim }}

    {%- endif -%}

{%- endmacro %}