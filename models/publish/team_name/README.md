### DBT Models

This is the directory where you will create your DBT models.  For more details, view the [DBT Models](https://docs.getdbt.com/docs/building-a-dbt-project/building-models) documentation.

You can safely remove this file from your project.